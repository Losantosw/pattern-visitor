 
package br.com.visitorpattern.interfaces;

import br.com.visitorpattern.classes.Computador;
import br.com.visitorpattern.classes.Monitor;
import br.com.visitorpattern.classes.Mouse;
import br.com.visitorpattern.classes.Teclado;

 
public interface ComputadorParteVisitor {
    public void visit(Computador computador);
    public void visit(Mouse mouse);
    public void visit(Teclado teclado);
    public void visit(Monitor monitor);
}


