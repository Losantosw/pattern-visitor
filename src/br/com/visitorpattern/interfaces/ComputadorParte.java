 
package br.com.visitorpattern.interfaces;

 
public interface ComputadorParte {
    public void accept(ComputadorParteVisitor computadorParteVisitor);
}
