
package br.com.visitorpattern.exe;

import br.com.visitorpattern.classes.Computador;
import br.com.visitorpattern.classes.ComputadorParteDisplayVisitor;
import br.com.visitorpattern.interfaces.ComputadorParte;


public class VisitorPattern {
    
    public static void main(String[] args) {
       ComputadorParte computador = new Computador();
       computador.accept(new ComputadorParteDisplayVisitor());
    }
    
}

