 
package br.com.visitorpattern.classes;
 
import br.com.visitorpattern.interfaces.ComputadorParteVisitor;

public class ComputadorParteDisplayVisitor implements ComputadorParteVisitor{
   @Override
   public void visit(Computador computador) {
      System.out.println("Exibindo Computador.");
   }

   @Override
   public void visit(Mouse mouse) {
      System.out.println("Exibindo Mouse.");
   }

   @Override
   public void visit(Teclado ceclado) {
      System.out.println("Exibindo Teclado.");
   }

   @Override
   public void visit(Monitor monitor) {
      System.out.println("Exibindo Monitor.");
   }
}


