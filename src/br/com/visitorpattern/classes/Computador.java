 
package br.com.visitorpattern.classes;

import br.com.visitorpattern.interfaces.ComputadorParte;
import br.com.visitorpattern.interfaces.ComputadorParteVisitor;
 
public class Computador implements ComputadorParte {
	
   ComputadorParte[] partes;

   public Computador(){
      partes = new ComputadorParte[] {new Mouse(), new Teclado(), new Monitor()};		
   } 

   @Override
   public void accept(ComputadorParteVisitor computadorParteVisitor) {
      for (int i = 0; i < partes.length; i++) {
         partes[i].accept(computadorParteVisitor);
      }
      computadorParteVisitor.visit(this);
   }
}

