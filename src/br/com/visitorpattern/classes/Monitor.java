 
package br.com.visitorpattern.classes;

import br.com.visitorpattern.interfaces.ComputadorParte;
import br.com.visitorpattern.interfaces.ComputadorParteVisitor;

 
public class Monitor implements ComputadorParte{
    @Override
    public void accept(ComputadorParteVisitor computadorParteVisitor) {
        computadorParteVisitor.visit(this);
    }
}
